import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World! Hello Human';
  }

  convert(celsius: number) {
    return {
      celsius: celsius,
      fahrenheit: (celsius * 9) / 5 + 32,
    };
  }
}
